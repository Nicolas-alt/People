import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import App from '../components/App';
import Form from '../components/form/Form';
import NotFound from '../components/NotFound';

export const Routes = _ => {
    return (
        <Router>
            <Switch>
                <Route exact path="/" component={App} />
                <Route exact path="/new" component={Form} />
                <Route exact path="/edit" component={Form} />
                <Route component={NotFound} />
            </Switch>
        </Router>
    );
}

export default Routes;
