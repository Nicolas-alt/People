import React from 'react'
import Card from './card/Card';
import './App.css';
import NavBar from './nav/NavBar';

export const App = _ => {
    return (
        <>
            <NavBar />
            <div className="container d-flex justify-content-center div--container">
                <Card />
            </div>
        </>
    );
}

export default App;
