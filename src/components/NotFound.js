import React from 'react'
import NavBar from './nav/NavBar';

export const NotFound = _ => {

  return (
    <>
      <NavBar />
      <h1>Ups!</h1>
    </>
  )
}

export default NotFound;
