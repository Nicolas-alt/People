import React from 'react'
import {Link} from 'react-router-dom'
import logo from '../../assets/svg/logo.svg'
import './navbar.css'

export const NavBar = _ => {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      <Link className="navbar-brand" to="/">
        <img src={logo} className="App-logo" alt="log" />
        People
      </Link>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className="collapse navbar-collapse" id="navbarText">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            < Link className="nav-link" to="/new">New</ Link>
          </li>
        </ul>
      </div>
    </nav>
  );
}

export default NavBar;
