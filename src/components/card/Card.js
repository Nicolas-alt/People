import React, {useState, useEffect} from 'react'
import axios from "axios"
import './card.css'

const url = 'https://emplooyee.herokuapp.com/api/v1/employee';

export const Card = _ => {

  const [data, setData] = useState([]);
  const getData = async () => {
    await axios.get(url)
      .then(response => {
        console.log(response.data);
        setData(response.data);
      })
  }

  useEffect(async () => {
    await getData()
  }, [])

  return (
    <>
      {data.map(
        dato => (
          <div className="div--card" key={dato.id}>
            <img src={dato.avatar} alt="User Image" />
            <p>
              {dato.first_name} {dato.last_name}
                  Email: {dato.email} <br />
                  City: {dato.city} <br />
                  Gender: {dato.gender} <br />
                  Hobie: {dato.hobie} <br />
                  Company: {dato.company} <br />
                  Job Title : {dato.job_title} <br />
                  Native lenguage: {dato.native_lenguage}
                Editar
                Eliminar
              </p>
          </div>
        ))}
    </>
  );
}

export default Card;

